﻿using GameClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Network.Senders
{
    public static class AuthorSender
    {
        public static void Login(string username, string password)
        {
            Client.Send((byte)1, new Dictionary<byte, object>
            {
                // author enum login
                {0, (byte)1 },
                {1,username },
                {2,password }
            });
        }
    }
}

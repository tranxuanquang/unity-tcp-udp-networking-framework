﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Network.Handlers
{
    interface OnPacketListener
    {
        bool OnPacketHandle(byte requestCode, Dictionary<byte, object> dt);
    }
}

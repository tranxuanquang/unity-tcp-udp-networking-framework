﻿using GameServer.Custom.Handlers;
using GameServer.Handlers;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace GameServer
{
    class World
    {
        public static World instance;
        public Dictionary<int, Client> clients;
        public List<OnPacketListener> handlers;

        public World()
        {
            instance = this;
            clients = new Dictionary<int, Client>();
            handlers = new List<OnPacketListener>();

            InitializeWorldData();
        }

        private void InitializeWorldData()
        {
            handlers.Add(new AuthorHandler());
        }

        public void OnHandlePacketReceived(byte requestCode, Dictionary<byte, object> dt, int clientId)
        {
            foreach (var handler in handlers)
            {
                if (handler.OnPacketHandle(requestCode, dt, clientId))
                {
                    break;
                }
            }
        }
    }
}

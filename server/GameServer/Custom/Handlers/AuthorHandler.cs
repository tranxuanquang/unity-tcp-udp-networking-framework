﻿using GameServer.Handlers;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.Custom.Handlers
{
    class AuthorHandler : OnPacketListener
    {
        public bool OnPacketHandle(byte requestCode, Dictionary<byte, object> dt, int client_id)
        {
            if (requestCode != 1) return false;
            Console.WriteLine(dt.Count);
            switch ((byte)dt[0])
            {
                case 1:
                    Login(dt, client_id);
                    break;
            }

            return true;
        }

        private void Login(Dictionary<byte, object> dt, int client_id)
        {
            Console.WriteLine("useranem: " + dt[1]);
            Console.WriteLine("password: " + dt[2]);
        }
    }
}

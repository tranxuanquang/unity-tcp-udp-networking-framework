﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameServer.Handlers
{
    interface OnPacketListener
    {
        bool OnPacketHandle(byte requestCode, Dictionary<byte, object> dt, int client_id);
    }
}
